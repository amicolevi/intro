<?php
    include "class.php";
?>
<html>
    <head>
        <title>Object Oriented PHP</title>
    </head>
    <body>
        <p>
        <?php
            $text = 'Hello World';
            echo "$text And The Universe";
            echo '<br>';
            $msg = new Message();
            echo Message:: $count;
            //echo $msg->text;
            $msg->show();    
            $msg1 = new Message("A new text");
            $msg1->show(); 
            echo Message:: $count;   
            $msg2 = new Message();
            $msg2->show(); 
            echo '<br>';
            echo Message:: $count;
            echo '<br>';
            // $msg3 = new redMessage('A red message');
            // $msg3->show();
            // echo '<br>';
            // $msg4 = new coloredMessage('A colored message');
            // $msg4->color = 'green';
            // $msg4->show('A green message');
            // echo '<br>';
            // $msg4->color = 'yellow';
            // $msg4->show('A green message');
            // echo '<br>';
            // $msg5 = new coloredMessage('a colored message');
            // $msg5->color = 'green';
            // $msg5->show();
            // echo '<br>';
            // showObject($msg5);
            // echo '<br>';
            // showObject($msg1);
        ?>
        </p>
    </body>
</html>