<?php
    class Message {
        protected $text = "A simple message";
        public static $count = 0;
        public function show(){
            echo "<p>$this->text</p>";
        }
        function __construct($text = ""){
            ++self:: $count;
            if($text != ""){
                $this->text = $text;
            }
        }
    }
    // class redMessage extends Message{
    //     public function show(){
    //         echo "<p style = 'color:red'>$this->text</p>";
    //     }
    }
    class coloredMessage extends Message{
        protected $color = 'red';
        public function __set($property,$value){
            if ($property == 'color'){
                $colors = array('red','yellow','green');
                if (in_array($value,$colors)){
                    $this->color = $value;
                }
            }
        }
        public function show(){
            echo "<p style = 'color:$this->color'>$this->text</p>";
        }
    }

    function showObject($object){
        $object->show(); 
    }


    // the 2nd homework as RONY solved
    class coloredpage extends Htmalpage{
        $colors = array('red','yellow','green');
        protected $color = 'red';
        public function __set($property, $value){
            if ($property == 'color'){
                if (in_array($value, $this->$colors)){
                    $this->color = $value;
                } else {
                    die("please select one of the allowed colors"); 
                }
            }
        }
        public function show(){
            echo "<html><head><title>$this->title</title></head><body><p style = 'color:$this->color'>$this->body<p></body></html>";
        }
    }
?>


